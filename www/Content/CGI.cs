﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using www.HTTP;
using www.Internals;

namespace www.Content
{
    class CGI
    {
        private Process p;
        private ProcessStartInfo startInfo;
        private string standardOutput = "";
        private string httpBody = "";
        private string httpHeader = "";
        private bool isInHeaderSection = true;

        public string HTTPBody { get => httpBody; }
        public string HTTPHeader { get => httpHeader; }

        public CGI()
        {

        }

        public bool RunCGI(string cgiPath, Socket s, VHost vh, HTTPRequest rq, HTTPResponse rp)
        {
            startInfo = new ProcessStartInfo();

            byte[] b = rq.getBody();

            startInfo.EnvironmentVariables.Add("AUTH_TYPE", "");
            startInfo.EnvironmentVariables.Add("CONTENT_LENGTH", b.Length.ToString()); //must, pokud je obsah
            startInfo.EnvironmentVariables.Add("CONTENT_TYPE", rq.getHeaderField("Content-Type"));
            startInfo.EnvironmentVariables.Add("GATEWAY_INTERFACE", "CGI/1.1");
            startInfo.EnvironmentVariables.Add("PATH_INFO", rq.GetRequestedFileAbsolute(vh));
            startInfo.EnvironmentVariables.Add("PATH_TRANSLATED", rq.GetRequestedFileAbsolute(vh)); //neexistuje obdoba rewrite, viditelná cesta je stejná
            startInfo.EnvironmentVariables.Add("QUERY_STRING", rq.requestedQueryString);
            startInfo.EnvironmentVariables.Add("REMOTE_ADDR", s.RemoteEndPoint.ToString());
            startInfo.EnvironmentVariables.Add("REMOTE_IDENT", "");
            startInfo.EnvironmentVariables.Add("REMOTE_USER", "");
            startInfo.EnvironmentVariables.Add("REQUEST_METHOD", rq.Method.ToString());
            startInfo.EnvironmentVariables.Add("SCRIPT_NAME", rq.requestedURI.Substring(1).Replace("?" + rq.requestedQueryString, ""));
            Debug.WriteLine(rq.requestedURI.Substring(1).Replace("?" + rq.requestedQueryString, ""));
            startInfo.EnvironmentVariables.Add("SERVER_NAME", vh.Name);
            startInfo.EnvironmentVariables.Add("SERVER_PORT", Program.settings.Port.ToString());
            startInfo.EnvironmentVariables.Add("SERVER_PROTOCOL", "HTTP/1.1");
            startInfo.EnvironmentVariables.Add("SERVER_SOFTWARE", "WWW");
            startInfo.FileName = cgiPath;
            startInfo.RedirectStandardInput = true;
            startInfo.RedirectStandardOutput = true;
            startInfo.UseShellExecute = false;

            p = Process.Start(startInfo);

            if(!p.HasExited)
            {
                if(b.Length > 0)
                {
                    using (StreamWriter w = p.StandardInput)
                    {
                        //w.AutoFlush = false;
                        w.Write(Encoding.ASCII.GetString(b));
                        //w.Flush();
                        //w.Close();
                        //w.Dispose();
                    }
                }

                p.BeginOutputReadLine();

                p.OutputDataReceived += P_OutputDataReceived;

                p.WaitForExit();
            
                /*string result = "";

                using (StreamReader sw = p.StandardOutput)
                {
                    result = sw.ReadToEnd();
                }*/

                //p.Close();

                return true;

            } return false;
        }

        private void P_OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            if(isInHeaderSection)
            {
                if(e.Data.Contains(": "))
                {
                    httpHeader += e.Data + Constants.HeaderSeparator;
                } else
                {
                    isInHeaderSection = false;
                }
            } else {
                httpBody += e.Data + Constants.HeaderSeparator;
            }
            standardOutput += e.Data;
        }
    }
}
