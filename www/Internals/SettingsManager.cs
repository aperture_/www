﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using www.HTTP;
using Newtonsoft.Json;

namespace www.Internals
{
    class SettingsManager
    {

        public static readonly string vhostSettingsFolder = "$internals/config/";
        public static readonly string vhostFolder = "$internals/hosts/";
        public static readonly string defaultsFolder = "$internals/defaults/";
        public static readonly string cgiFolder = "$internals/cgi/";

        public static Dictionary<string, string> cgiPaths = new Dictionary<string, string>();

        public static void VhostConfiguration(VHost vh)
        {
            if (!Directory.Exists(vhostSettingsFolder + vh.Name))
            {
                Directory.CreateDirectory(vhostSettingsFolder + vh.Name);
            }
        }

        public static Dictionary<int, string> responseCodes = new Dictionary<int, string>();

        public static List<VHost> GetVhosts()
        {
            List<VHost> hosts = new List<VHost>();

            if(Directory.Exists(vhostFolder))
            {
                foreach(string file in Directory.GetFiles(vhostFolder))
                {
                    hosts.Add(JsonConvert.DeserializeObject<VHost>(File.ReadAllText(Path.GetFullPath(file))));
                }
            }

            return hosts;
        }
        /// <summary>
        /// Vrací odpověď na OPTIONS dotaz pro celý server, ne specifický soubor.
        /// </summary>
        /// <param name="rq">Příchozí request</param>
        /// <param name="rp">Odchozí response</param>
        public static void GetOptionsResponse(HTTPRequest rq, HTTPResponse rp)
        {
            rp.AddHeader("Allow", "HEAD, GET, POST, PUT, DELETE, TRACE, OPTIONS");
        }

        public static ServerSettings GetServerSettings()
        {
            return JsonConvert.DeserializeObject<ServerSettings>(File.ReadAllText(Path.GetFullPath(defaultsFolder + "config.json")));
        }

        public static bool LoadResponseCodes()
        {
            responseCodes = JsonConvert.DeserializeObject<Dictionary<int, string>>(File.ReadAllText(Path.GetFullPath(defaultsFolder + "responseText.json")));
            return (responseCodes.Count > 0);
        }

        public static string GetResponseMessage(int responseCode)
        {
            string message = responseCodes.Where(x => x.Key == responseCode).FirstOrDefault().Value;
            return (message != null) ? message : "";
        }

        public static string GetCgiPath(string cgiName)
        {
            if(cgiPaths.Count == 0)
            {
                cgiPaths = JsonConvert.DeserializeObject<Dictionary<string, string>>(File.ReadAllText(Path.GetFullPath(defaultsFolder + "cgi.json")));
            }

            return (cgiPaths.ContainsKey(cgiName)) ? Path.GetFullPath(cgiFolder + cgiPaths[cgiName]) : "";
        }
        
    }
}
