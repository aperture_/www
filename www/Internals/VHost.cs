﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using www.HTTP;
using Newtonsoft.Json;

namespace www.Internals
{
    class VHost
    {
        /// <summary>
        /// Doména "virtualhostu". Název složky, ve které se bude hledat konfigurace.
        /// </summary>
        [JsonProperty]
        private string name;

        /// <summary>
        /// Absolutní cesta ke složce.
        /// </summary>
        [JsonProperty]
        private string basePath;

        /// <summary>
        /// Zda se má výstup komprimovat pomocí GZIPu.
        /// </summary>
        [JsonProperty]
        private bool useGzip = false;

        [JsonIgnore]
        public string Name { get => name; }

        [JsonIgnore]
        public string BasePath { get => basePath; }

        [JsonIgnore]
        public bool UseGzip { get => useGzip; }

        public Encoding defaultEncoding = Encoding.Default;

        /// <summary>
        /// Tento soubor bude defaultně hledán, pokud nebude specifikován žádný soubor.
        /// </summary>
        public string defaultFile = "";

        /// <summary>
        /// Seznam souborů, které se budou defaultně zobrazovat pro určitý stavový kód (pokud budou existovat).
        /// Cesta bude zadaná absolutně. Určeno pro kód 4xx a 5xx. 
        /// </summary>
        [JsonProperty]
        public Dictionary<int, string> stateFiles = new Dictionary<int, string>();
        
        /// <summary>
        /// Mapování přípon na CGI. 
        /// </summary>
        public Dictionary<string, string> cgiBindings = new Dictionary<string, string>();

        [JsonIgnore]
        private Dictionary<string, string> mimeBindings = new Dictionary<string, string>();

        [JsonIgnore]
        public string defaultMime = "application/octet-stream";

        [JsonProperty]
        public List<string> boundNames = new List<string>();

        [JsonProperty]
        public bool AcceptDelete = true;

        [JsonProperty]
        public bool AcceptPut = true;

        //přidat "listen domény" - > může být nabindováno více domén na jeden vhost

        public VHost(string name)
        {
            this.name = name;
            this.basePath = "";
            Init();
        }

        [JsonConstructor]
        public VHost(string name, string folder, bool useGzip)
        {
            this.name = name;
            this.basePath = folder;
            this.useGzip = useGzip;

            Init();
            Program.Logger.Info("VHost " + name + " initialized.");
        }

        private void Init()
        {
            this.mimeBindings = Defaults.GetDefaultMimes();
            Internals.SettingsManager.VhostConfiguration(this);
        }

        public void SaveVHost()
        {
            File.WriteAllText(@"$internals\hosts\" + name + ".json", Newtonsoft.Json.JsonConvert.SerializeObject(this));
        }

        public bool IsFolder(string path)
        {
            try
            {
                return (File.GetAttributes(basePath + path).HasFlag(FileAttributes.Directory));
            } catch (Exception ex)
            {

            }

            return false;
        }

        public bool FileExists(string file)
        {
            //tohle by se mělo brát z konfigurace
            return File.Exists(basePath + file);
        }

        public string FileLastModified(string file)
        {
            return File.GetLastWriteTime(file).ToString("r");
        }

        public bool CreateFile(string file, byte[] content)
        {
            try {

                if(!Directory.Exists(Path.GetDirectoryName(basePath + file)))
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(basePath + file));
                }

                File.WriteAllBytes(basePath + file, content);

                return true;
            } catch (Exception e) {
                Program.Logger.Error(e);
                return false;
            }
        }

        public bool DeleteFile(string file)
        {
            try {

                File.Delete(basePath + file);
                return true;

            } catch {
                return false;
            }
        }

        /// <summary>
        /// Dle přípony zjistí, zda se soubor nachází v CGI binding, tj. že se má zpracovat pomocí CGI.
        /// Předpokládá existenci souboru!
        /// </summary>
        /// <param name="file">Cesta k souboru</param>
        /// <returns>Soubor je CGI, nebo ne</returns>
        public bool IsStatic(string file)
        {
            string extension = Path.GetExtension(basePath + file);
            return !cgiBindings.ContainsKey(extension); //dle přípony rozpozná, zda se jedná o statický soubor nebo ne
        }

        public string getCgiType(string file)
        {
            string extension = Path.GetExtension(basePath + file);
            return cgiBindings[extension]; //dle přípony rozpozná, zda se jedná o statický soubor nebo ne
        }

        public void AddDate(HTTPResponse rp)
        {
            rp.AddHeader("Date", DateTime.Now.ToUniversalTime().ToString("r")); //RFC formát
        }

        internal void AddDefaultHeaders(HTTPResponse rp)
        {
            rp.AddHeader("Date", DateTime.Now.ToUniversalTime().ToString("r")); //RFC formát
            rp.AddHeader("Connection", "Close");
            rp.AddHeader("Cache-Control", "no-cache");
            //throw new NotImplementedException();
        }

        public byte[] GetFileContent(string file)
        {
            return File.ReadAllBytes(basePath + file);
        }

        public void HandleOptionsForResource(string file, HTTPResponse r)
        {
            List<string> allowedMethods = new List<string>();

            if(IsFolder(file))
            {

                if (AcceptDelete) allowedMethods.Add("DELETE");

            } else { 

                if (AcceptPut) allowedMethods.Add("PUT");

                if (FileExists(file))
                {
                    if (AcceptDelete) allowedMethods.Add("DELETE");
                    allowedMethods.Add("GET");

                    if (IsStatic(file))
                    {
                        //statické soubory odpovídají na GET, HEAD, OPTIONS, POST není dovolen
                        allowedMethods.Add("HEAD"); //head má smysl jen na statické soubory?
                    } else {
                        allowedMethods.Add("POST");
                    }

                }
            }

            allowedMethods.Add("TRACE");
            allowedMethods.Add("OPTIONS");

            r.AddHeader("Allow", String.Join(", ", allowedMethods));

        }

        public string GetContentType(string file)
        {
            //File.GetAttributes(file);
            //return "text/html";
            string extension = Path.GetExtension(basePath + file);
            //todo definice z JSONu

            string mimeType = mimeBindings.Where(x => x.Key == extension).FirstOrDefault().Value;

            if(mimeType != null)
            {
                return mimeType;
            }

            return defaultMime;
        }

        public Encoding GetEncoding()
        {
            //html
            return defaultEncoding;
        }
        
    }
}
