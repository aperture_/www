﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace www.Internals
{
    class ServerSettings
    {
        [JsonProperty]
        private int port;

        public int Port { get => port; }

        [JsonConstructor]

        public ServerSettings(int port)
        {
            this.port = port;
            Program.Logger.Info("Server settings initialized");
        }

    }
}
