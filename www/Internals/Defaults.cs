﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;

namespace www.Internals
{
    class Defaults
    {

        public static string defaultsFolder = "$internals/defaults/";

        /// <summary>
        /// Pokusi se nalezt soubor s mapovanim mime typu na priponu. Pokud nenalezne, vraci prazdny dictionary.
        /// </summary>
        /// <returns>MIME Dictionary / prazdne Dictionary</returns>
        public static Dictionary<string, string> GetDefaultMimes()
        {
            Dictionary<string, string> mimes = new Dictionary<string, string>();

            if(File.Exists(defaultsFolder + "mimes.json"))
            {
                string mimeList = File.ReadAllText(defaultsFolder + "mimes.json");
                mimes = JsonConvert.DeserializeObject<Dictionary<string, string>>(mimeList);
            } else {
                Program.Logger.Error("Default mime / extension binding file was not found in " + (Path.GetFullPath(defaultsFolder) + "mimes.json") );
            }

            return mimes;
        }

        public static Dictionary<int, string> GetDefaultResponseTexts()
        {
            Dictionary<int, string> responseTexts = new Dictionary<int, string>();

            if (File.Exists(defaultsFolder + "responseText.json"))
            {
                string rList = File.ReadAllText(defaultsFolder + "responseText.json");
                responseTexts = JsonConvert.DeserializeObject<Dictionary<int, string>>(rList);
            }
            else
            {
                Program.Logger.Error("Default mime / extension binding file was not found in " + (Path.GetFullPath(defaultsFolder) + "mimes.json"));
            }

            return responseTexts;
        }

    }
}
