﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net.Sockets;
using System.IO;
using www.HTTP;
using System.Diagnostics;
using www.Internals;
using System.Linq;
using www.Content;
using www.HTTP.Headers;

namespace www
{
    class RequestHandler
    {
        Thread requestThread;
        TcpClient socket;

        public RequestHandler(TcpClient clientSocket)
        {
            this.socket = clientSocket;
            requestThread = new Thread(ProcessRequest);
            requestThread.Start();
        }

        public VHost FindVHost(string domain)
        {
            return Program.hosts.Find(x => x.Name.ToLower() == domain.ToLower());
        }

        private void ProcessRequest()
        {
            bool doRunThread = true;
            bool isRequestComplete = false;
            HTTPRequest rq = HTTPRequest.Empty;
            HTTPResponse rp; //socket.Client.RemoteEndPoint

            using (NetworkStream ns = socket.GetStream())
            {
                while (doRunThread)
                {
                    if (ns.CanRead/* && ns.DataAvailable*/)
                    {

                        using (MemoryStream ms = new MemoryStream())
                        {
                            bool continueReading = true;
                            int crCount = 0;
                            int lfCount = 0;

                            while (continueReading)
                            {
                                while (ns.DataAvailable)
                                {
                                    byte data = (byte)ns.ReadByte();

                                    if (data == Constants.bCR) {
                                        crCount++;
                                    } else if (data == Constants.bLF) {
                                        lfCount++;
                                    } else {
                                        crCount = lfCount = 0;
                                    }

                                    //Debug.WriteLine("CR:" + crCount + "LF:" + lfCount);

                                    ms.WriteByte(data);

                                    if (crCount == 2 && lfCount == 2)
                                    {
                                        continueReading = false;
                                    }
                                    //tady zjistit, zda se dvakrat vyskytlo CR LF CR LF
                                }

                            }

                            rq = new HTTPRequest(ms.ToArray());

                            isRequestComplete = true;
                        }

                        //ns.Read(receiveBuffer, 0, receiveBuffer.Length);

                        // dle typu requestu - při GETu lze další čtení přeskočit

                    }

                    if(ns.CanWrite && isRequestComplete)
                    {
                        rp = new HTTPResponse();

                        if(rq.IsValid)
                        {
                            if(rq.HasMethod)
                            {

                                if(rq.Version == HTTP.Version.Unsupported)
                                {
                                    rp.httpState = HTTPState.STATE_505;
                                } else {
                                    string requestedFile = rq.GetRequestedFile();

                                    if(requestedFile == "*" && rq.Method == HTTPMethod.OPTIONS)
                                {
                                    rp.setBody(new byte[0]);
                                    rp.requestedMethod = HTTPMethod.OPTIONS;
                                    SettingsManager.GetOptionsResponse(rq, rp);
                                    //rp.
                                } else {

                                    string requestedHost = rq.getRequestedHost();

                                    VHost vh = FindVHost(requestedHost);

                                    if (vh == null)
                                    {
                                        rp.httpState = HTTPState.STATE_400; //pokud neexistuje host, vrati 400 - Bad Request
                                    } else {

                                        rp.requestedMethod = rq.Method;

                                        if (vh.IsFolder(rq.GetRequestedFile()))
                                        {
                                            rq.AppendIndex(vh.defaultFile);
                                        }

                                        if (vh.IsStatic(rq.GetRequestedFile()))
                                        {

                                        switch(rq.Method)
                                        {
                                            case HTTPMethod.CONNECT:
                                            {
                                                rp.httpState = HTTPState.STATE_501;
                                                break;
                                            }
                                            case HTTPMethod.GET:
                                            {
                                                HandleGet(vh, rq, rp);
                                                break;
                                            }
                                            case HTTPMethod.OPTIONS:
                                            {
                                                HandleOptions(vh, rq, rp);
                                                break;
                                            }
                                            case HTTPMethod.HEAD:
                                            {
                                                HandleHead(vh, rq, rp);
                                                break;
                                            }
                                            case HTTPMethod.TRACE:
                                            {
                                                HandleTrace(rq, rp);
                                                break;
                                            }
                                            case HTTPMethod.DELETE:
                                            {
                                                HandleDelete(vh, rq, rp);
                                                break;
                                            }
                                            case HTTPMethod.PUT:
                                            {
                                                HandlePut(vh, rq, rp);
                                                break;
                                            }
                                            case HTTPMethod.POST:
                                            {
                                                rp.httpState = HTTPState.STATE_405;
                                                break;
                                            }
                                            case HTTPMethod.NONE:
                                            default:
                                            {
                                                rp.httpState = HTTPState.STATE_501;
                                                break;
                                            }
                                        }

                                        } else {

                                            if(vh.FileExists(rq.GetRequestedFile()))
                                            {
                                                CGI c = new CGI();

                                                if(c.RunCGI(SettingsManager.GetCgiPath(vh.getCgiType(rq.GetRequestedFile())), socket.Client, vh, rq, rp))
                                                {
                                                    HandleCGI(c, rp);
                                                }

                                            } else {
                                                rp.httpState = HTTPState.STATE_404;
                                            }

                                        }
                                    }
                                }
                                }

                            } else {
                                rp.httpState = HTTPState.STATE_405;
                            } 

                        } else {
                            rp.httpState = HTTPState.STATE_400; //bad request, tj. není validní
                        }

                        byte[] header = rp.getHeader();
                        byte[] data = rp.getBody();


                        ns.Write(header, 0, header.Length);
                        ns.Write(data, 0, data.Length);
                        socket.Close();

                        Program.Logger.Info("Sent response " + rp.getHTTPStatus());

                        doRunThread = false;
                    }
                }
            }

        }

        private void HandleCGI(CGI c, HTTPResponse rp)
        {
            string[] headers = c.HTTPHeader.Split(new string[] { Constants.HeaderSeparator }, StringSplitOptions.None);

            foreach(string h in headers)
            {
                if(h != "") rp.AddHeader(new HTTPHeader(h));
            }

            string contentType = rp.getHeaderField("Content-type");
            Encoding encoding = Encoding.Default;

            if (contentType != "")
            {
                if (contentType.Contains("charset="))
                {
                    int charsetIndex = contentType.IndexOf("charset=");
                    int nextStop = contentType.IndexOf(";", charsetIndex) - 1;
                    if (nextStop < 0) nextStop = contentType.Length - 1;

                    string encodingText = contentType.Substring(charsetIndex, nextStop - charsetIndex).ToLower();

                    switch (encodingText)
                    {
                        case "utf-8": { encoding = Encoding.UTF8; break; }
                    }
                }
            }

            rp.setBody(encoding.GetBytes(c.HTTPBody));

            //rp.setHeader(Encoding.ASCII.GetBytes(c.HTTPHeader));
            //rp.setBody(c.HTTPBody);

            /*foreach(string l in lines)
            {
                length += Encoding.ASCII.GetByteCount(l) + 2;
                if (l.Contains(": ")) rp.Headers.Add(new HTTP.Headers.HTTPHeader(l));
                if (l == "") break;
            }

            string contentType = rp.getHeaderField("Content-type");
            Encoding encoding = Encoding.Default;

            if(contentType != "")
            {
                if(contentType.Contains("charset="))
                {
                    int charsetIndex = contentType.IndexOf("charset=");
                    int nextStop = contentType.IndexOf(";", charsetIndex) - 1;
                    if (nextStop < 0) nextStop = contentType.Length - 1;

                    string encodingText = contentType.Substring(charsetIndex, nextStop - charsetIndex).ToLower();

                    switch(encodingText)
                    {
                        case "utf-8": { encoding = Encoding.UTF8; break; }
                    }
                }
            }*/

        }

        private void HandlePut(VHost vh, HTTPRequest rq, HTTPResponse rp)
        {
            if (vh.AcceptPut)
            {
                if(rq.getHeaderField("Content-Range") == "")
                {
                    if(vh.IsFolder(rq.GetRequestedFile()))
                    {

                        rp.httpState = HTTPState.STATE_301; //uživatel by se snažil zapsat do složky

                    } else {

                        if (!vh.FileExists(rq.GetRequestedFile()))
                        {
                            if (vh.CreateFile(rq.GetRequestedFile(), rq.getBody()))
                            {
                                rp.httpState = HTTPState.STATE_201;
                                rp.AddHeader("Location", rq.GetRequestedFile());
                            }
                            else {
                                rp.httpState = HTTPState.STATE_500;
                            }

                        } else {

                            if (vh.CreateFile(rq.GetRequestedFile(), rq.getBody()))
                            {
                                rp.httpState = HTTPState.STATE_200; //modifikace má vracet 201
                                rp.AddHeader("Location", rq.GetRequestedFile());
                            } else {
                                rp.httpState = HTTPState.STATE_500;
                            }

                        }
                    }
                } else {
                    rp.httpState = HTTPState.STATE_501;
                }
            } else {
                rp.httpState = HTTPState.STATE_403;
            }
        }

        private void HandleDelete(VHost vh, HTTPRequest rq, HTTPResponse rp)
        {
            if(vh.AcceptDelete)
            {
                if(vh.FileExists(rq.GetRequestedFile()))
                {

                    if (vh.DeleteFile(rq.GetRequestedFile()))
                    {
                        rp.httpState = HTTPState.STATE_200;
                    }

                } else {
                    rp.httpState = HTTPState.STATE_404;
                }

            } else {
                rp.httpState = HTTPState.STATE_403;
            }
        }

        private void HandleGet(VHost vh, HTTPRequest rq, HTTPResponse rp)
        {
        
            if (vh.FileExists(rq.GetRequestedFile()))
            {
                vh.AddDefaultHeaders(rp);
                rp.AddHeader("Content-Type", vh.GetContentType(rq.GetRequestedFile()));
                rp.setBody(vh.GetFileContent(rq.GetRequestedFile()));
                rp.AddHeader("Content-Length", rp.getBody().Length.ToString());
                rp.AddHeader("Last-Modified", vh.FileLastModified(rq.GetRequestedFile()));
            } else {
                rp.httpState = HTTPState.STATE_404;
            }

        }

        private void HandleOptions(VHost vh, HTTPRequest rq, HTTPResponse rp)
        {
            vh.HandleOptionsForResource(rq.GetRequestedFile(), rp);
        }

        private void HandleHead(VHost vh, HTTPRequest rq, HTTPResponse rp)
        {
            
            if (vh.FileExists(rq.GetRequestedFile()))
            {
                vh.AddDefaultHeaders(rp);
                rp.AddHeader("Content-Type", vh.GetContentType(rq.GetRequestedFile()));
                rp.AddHeader("Content-Length", vh.GetFileContent(rq.GetRequestedFile()).Length.ToString());
                rp.AddHeader("Last-Modified", vh.FileLastModified(rq.GetRequestedFile()));
            } else {
                rp.httpState = HTTPState.STATE_404;
            }
        }

        private void HandleTrace(HTTPRequest rq, HTTPResponse rp)
        {
            rp.setBody(rq.getHeader().Concat(rq.getBody()).ToArray());
            rp.httpState = HTTPState.STATE_200;
            rp.AddHeader("Content-Type", "message/http");
        }
    }
}
