﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace www
{
    class Constants
    {
        public const string HeaderSeparator = "\r\n";
        public const int ExitCodeError = -1;

        public const char CR = '\r';
        public const char LF = '\n';

        public const byte bCR = (byte)'\r';
        public const byte bLF = (byte)'\n';

    }
}
