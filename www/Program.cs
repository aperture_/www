﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net.Sockets;
using System.Net;
using www.Internals;
using log4net;

namespace www
{
    class Program
    {

        public static List<VHost> hosts = new List<VHost>();
        private static bool doRun = true;
        private static IPAddress ListenAddress = IPAddress.Any;
        public static ServerSettings settings;
        public static readonly ILog Logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        static void Main(string[] args)
        {
            settings = SettingsManager.GetServerSettings();
            hosts = SettingsManager.GetVhosts();

            if(settings == null)
            {
                Logger.Error("No settings found.");
                Environment.Exit(Constants.ExitCodeError);
            }

            if (hosts.Count == 0)
            {
                Logger.Error("No hosts detected.");
                Environment.Exit(Constants.ExitCodeError);
            }

            if (!SettingsManager.LoadResponseCodes())
            {
                Logger.Warn("No response messages found. Response messages will be empty.");
            }

            try
            {
                TcpListener l = new TcpListener(new IPEndPoint(ListenAddress, settings.Port));
                l.Start();

                Logger.Info("Server listening on " + ListenAddress.ToString() + " port " + settings.Port);

                TcpClient incomingSocket = new TcpClient();

                while (doRun)
                {
                    incomingSocket = l.AcceptTcpClient();
                    Logger.Info("Received request, parsing");

                    RequestHandler handler = new RequestHandler(incomingSocket);
                }

            } catch (Exception e)
            {
                Logger.Error(e.ToString());
                Environment.Exit(Constants.ExitCodeError);
            }
        }
    }
}
