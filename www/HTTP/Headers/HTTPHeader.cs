﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace www.HTTP.Headers
{
    class HTTPHeader
    {

        protected string name;
        protected string value;
        protected bool isValid;

        public bool IsValid { get => isValid; }

        public HTTPHeader(string headerValue)
        {
            headerValue.Replace("\r\n", "");
            if(headerValue.Contains("HTTP"))
            {

            } else {
                string[] values = headerValue.Split(':');

                this.name = values[0];
                if(values[1].Length > 0)
                {
                    isValid = true;
                    this.value = values[1].Substring(1, values[1].Length - 1); //Vždy odděleno mezerou
                } else {
                    isValid = false;
                    this.value = "";
                }

            }
        }

        public HTTPHeader(string name, string value)
        {
            this.name = name;
            this.value = value;
        }

        public string Name
        {
            get
            {
                return name;
            }
        }

        public string Value
        {
            get
            {
                return value;
            }
        }
    }

    public enum HeaderType
    {
        HTTPV10M,
        HTTPV11M
    }

}
