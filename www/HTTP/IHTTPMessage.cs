﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace www.HTTP
{
    interface IHTTPMessage
    {

        byte[] getBody();
        byte[] getHeader();

    }
}
