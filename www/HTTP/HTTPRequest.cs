﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using www.HTTP.Headers;
using www.Internals;

namespace www.HTTP
{
    class HTTPRequest : HTTPMessage
    {

        private bool hasMethod = false;
        private bool isValid = false;
        private string requestedFile = "index.html";
        public string requestedQueryString = ""; //mimo CGI nemá dopad na nic
        public string requestedURI = "";

        private byte[] httpBody;

        private HTTPMethod method = HTTPMethod.NONE;
        public Version Version { get => HTTPVersion; }

        public HTTPMethod Method
        {
            get
            {
                return method;
            }
        }

        public static HTTPRequest Empty
        {
            get
            {
                return new HTTPRequest(new byte[0]);
            }
        }

        public bool IsValid
        {
            get
            {
                return isValid;
            }
        }

        public bool HasMethod
        {
            get
            {
                return hasMethod;
            }
        }

        #region IHTTPMessage

        public override byte[] getBody()
        {
            if (httpBody == null) return new byte[0];
            return httpBody;
        }

        public override byte[] getHeader()
        {
            StringBuilder sb = new StringBuilder();

            foreach (Headers.HTTPHeader h in headers)
            {
                sb.Append(h.Name + ": " + h.Value + Constants.HeaderSeparator);
            }

            sb.Append(Constants.HeaderSeparator);

            return Encoding.ASCII.GetBytes(sb.ToString());
        }

        #endregion

        public void setBody(byte[] content)
        {
            httpBody = content;
        }

        public string getRequestedHost()
        {
            HTTPHeader h = headers.Find(x => x.Name.ToLower() == "host");
            return (h != null) ? h.Value : "";
        }

        private bool getHTTPMethod(string method)
        {
            switch(method)
            {
                case "POST": { this.method = HTTPMethod.POST; break; }
                case "GET": { this.method = HTTPMethod.GET; break; }
                case "OPTIONS": { this.method = HTTPMethod.OPTIONS; break; }
                case "HEAD": { this.method = HTTPMethod.HEAD; break; }
                case "CONNECT": { this.method = HTTPMethod.CONNECT; break; }
                case "PUT": { this.method = HTTPMethod.PUT; break; }
                case "DELETE": { this.method = HTTPMethod.DELETE; break; }
                case "TRACE": { this.method = HTTPMethod.TRACE; break; }
                default: { this.method = HTTPMethod.NONE; return false; }
            }

            return true;

        }
         
        private string getRequestedFile(string queryString)
        {
            if(queryString.Contains("?")) //rozdělení na dotaz a dotaz s QUERY
            {
                string[] parsedQuery = queryString.Split('?');
                requestedQueryString = parsedQuery[1];
                return parsedQuery[0];
            }

            return queryString;
        }

        public HTTPRequest(byte[] request)
        {
            headers = new List<HTTPHeader>();
            bool isHeadersSector = false;

            string requestAscii = Encoding.ASCII.GetString(request);
            int len = 0;

            string[] headersText = requestAscii.Split(new[] { "\r\n" }, StringSplitOptions.None);

            for(int i = 0; i < headersText.Length; i++)
            {
                len += Encoding.ASCII.GetBytes(headersText[i]).Length + 2;

                if (i == 0) { 

                    if (headersText[i].Contains("HTTP"))
                    {
                        string[] methodSection = headersText[i].Split(' ');

                        if (methodSection.Length >= 2)
                        {

                            isValid = true;

                            this.requestedFile = getRequestedFile(methodSection[1]);

                            requestedURI = methodSection[1];

                            getHTTPMethod(methodSection[0]);

                            getHTTPVersion(methodSection[2]);

                            if(this.method != HTTPMethod.NONE)
                            {
                                hasMethod = true;
                            }

                        } else {

                            isValid = false;
                            //invalid request!
                        }
                    } else
                    {
                        isValid = false;
                    }
                }

                //Přidá se hlavička jen pokud má správný formát a existuje hlavička
                if(!isHeadersSector && i > 0)
                {
                    if (headersText[i].Contains(": ") && hasMethod)
                    {
                        HTTPHeader h = new HTTPHeader(headersText[i]);
                        
                        if(h.IsValid)
                        {
                            headers.Add(h);
                        } else {
                            isValid = false;
                        }

                    } else
                    {
                        break;
                    }
                } 
            }

            setBody(request.Skip(len).ToArray());
        }

        private void getHTTPVersion(string version)
        {
            switch(version)
            {
                case "HTTP/1.1":
                {
                    HTTPVersion = Version.V11;
                    break;
                }
                case "HTTP/1.0":
                {
                    HTTPVersion = Version.Unsupported;
                    break;
                }
                case "HTTP/2.0":
                {
                    HTTPVersion = Version.Unsupported;
                    break;
                }
                default:
                {
                    HTTPVersion = Version.Unsupported;
                    break;
                }
            }
        }

        public string GetRequestedFile()
        {
            return requestedFile;
        }

        public string GetRequestedFileAbsolute(VHost vh)
        {
            return Path.GetFullPath(vh.BasePath + requestedFile);
        }

        public void AppendIndex(string filename)
        {
            requestedFile += ((requestedFile[requestedFile.Length - 1] == '/')?"":"/") + filename;
        }

    }

    public enum HTTPMethod
    {
        NONE, //-pro nevalidní requesty
        GET,
        POST,
        HEAD,
        OPTIONS,
        DELETE,
        PUT,
        CONNECT,
        TRACE
    }

}
