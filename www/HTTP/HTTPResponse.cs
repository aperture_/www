﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using www.HTTP.Headers;

namespace www.HTTP
{
    class HTTPResponse : HTTPMessage, IHTTPMessage
    {
        protected Version httpVersion = Version.V11;
        public HTTPState httpState = HTTPState.STATE_200;

        public int httpStateOverride = -1; //pro CGI
        public HTTPMethod requestedMethod = HTTPMethod.NONE; //hraje roli pouze pro head a options

        public string responseText = "";

        public byte[] httpBody = new byte[0];

        public HTTPResponse()
        {
            headers = new List<Headers.HTTPHeader>();
        }

        public override byte[] getBody()
        {
            return httpBody; 
        }

        public void setBody(byte[] content)
        {
            httpBody = content;
        }

        public string getHTTPStatus()
        {
            string status = "";

            switch (httpVersion)
            {
                case Version.V11:
                {
                    status = "HTTP/1.1";
                }

                break;
            }

            int statusCode = (httpStateOverride >= 100) ? httpStateOverride : (int)httpState;

            return status + " " + (statusCode).ToString() + " " + Internals.SettingsManager.GetResponseMessage(statusCode);
        }

        public override byte[] getHeader()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(getHTTPStatus() + Constants.HeaderSeparator);

            foreach(Headers.HTTPHeader h in headers)
            {
                sb.Append(h.Name + ": " + h.Value + Constants.HeaderSeparator);
            }

            sb.Append(Constants.HeaderSeparator);

            return Encoding.ASCII.GetBytes(sb.ToString());
        }

        public void AddHeader(string name, string value)
        {
            headers.Add(new Headers.HTTPHeader(name, value));
        }

        internal void AddHeader(HTTPHeader header)
        {
            headers.Add(header);
        }
    }

    /// <summary>
    /// Často používané kódy odpovědí.
    /// </summary>
    public enum HTTPState
    {
        /// <summary>
        /// OK
        /// </summary>
        STATE_200 = 200, //ok
        /// <summary>
        /// Bad Request
        /// </summary>
        STATE_400 = 400, //bad request
        /// <summary>
        /// Not Found
        /// </summary>
        STATE_404 = 404, //not found
        /// <summary>
        /// Bad Method
        /// </summary>
        STATE_405 = 405, //bad method
        /// <summary>
        /// Not implemented
        /// </summary>
        STATE_501 = 501, //not implemented
        /// <summary>
        /// Forbidden
        /// </summary>
        STATE_403 = 403,
        /// <summary>
        /// Accepted
        /// </summary>
        STATE_201 = 201,
        /// <summary>
        /// Internal server error.
        /// </summary>
        STATE_500 = 500,
        /// <summary>
        /// Moved permanently
        /// </summary>
        STATE_301 = 301,
        /// <summary>
        /// HTTP Version Not Supported
        /// </summary>
        STATE_505 = 505
    }
}
