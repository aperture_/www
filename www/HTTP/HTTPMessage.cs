﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using www.HTTP.Headers;

namespace www.HTTP
{
    abstract class HTTPMessage : IHTTPMessage
    {

        protected Version HTTPVersion;

        public abstract byte[] getBody();

        public abstract byte[] getHeader();

        protected List<HTTPHeader> headers;

        public List<HTTPHeader> Headers { get => headers; }

        public void setHeaders(List<HTTPHeader> headers)
        {
            this.headers = headers;
        }

        public string getHeaderField(string key)
        {
            HTTPHeader h = headers.Where(x => x.Name.ToLower() == key.ToLower()).FirstOrDefault();
            return (h != null) ? h.Value : "";
        }

        //https://tools.ietf.org/html/rfc1945#section-4
        //https://stackoverflow.com/questions/3825390/effective-way-to-find-any-files-encoding
    }

    public enum Version
    {
        V10, //1.0
        V11, //1.1
        V20,   //2.0
        Unsupported
    };
}
