﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace TCPClient
{
    class Program
    {
        static void Main(string[] args)
        {
            TcpClient c = new TcpClient();
            bool isEndpointSet = false;
            bool isConnected = false;
            bool doDisconnect = false;

            Console.WriteLine("Napište hostname a následně port");
            do
            {
                try
                {
                    Console.WriteLine("Hostname");
                    string hostname = Console.ReadLine();
                    Console.WriteLine("Port");
                    int port = Convert.ToInt32(Console.ReadLine());
                    c = new TcpClient(hostname, port);

                    if(c.Connected)
                    {
                        Console.WriteLine("Úspěšně připojeno.");
                        isConnected = true;
                    }

                    isEndpointSet = true;
                } catch (Exception e){
                    Console.WriteLine(e.ToString());
                }
            } while (!(isEndpointSet && isConnected));

            bool isReceiving = false;

            using (NetworkStream ns = c.GetStream())
                {
                    while (!doDisconnect)
                    {

                        if (ns.CanWrite)
                        {
                            bool doContinue = false;
                            string input = "";

                            while (doContinue)
                            {
                                Console.Write(">");
                                input += Console.ReadLine();
                            }

                            byte[] bInput = Encoding.ASCII.GetBytes(input);
                            ns.Write(bInput, 0, bInput.Length);

                    }


                    if (!c.Connected)
                        {
                            Console.WriteLine("Spojení ukončeno");
                            doDisconnect = true;
                        }
                        //pokus o čtení
                        if(ns.CanRead)
                        {
                            using (MemoryStream ms = new MemoryStream())
                            {
                                while(ns.DataAvailable)
                                {
                                    ms.WriteByte((byte)ns.ReadByte());
                                }

                                if(ms.Length > 0)
                                {
                                    Console.WriteLine("<" + Encoding.ASCII.GetString(ms.ToArray()));
                                }
                            }
                        }

                    }
                }
                       
        }
    }
}
