\select@language {czech}
\contentsline {part}{I\hspace {1em}Protokoly}{2}
\contentsline {section}{\numberline {1}HTTP}{2}
\contentsline {section}{\numberline {2}Metody}{2}
\contentsline {subsubsection}{\numberline {2.0.1}GET}{2}
\contentsline {subsubsection}{\numberline {2.0.2}HEAD}{2}
\contentsline {subsubsection}{\numberline {2.0.3}POST}{2}
\contentsline {subsubsection}{\numberline {2.0.4}PUT}{2}
\contentsline {subsubsection}{\numberline {2.0.5}DELETE}{2}
\contentsline {subsubsection}{\numberline {2.0.6}OPTIONS}{3}
\contentsline {subsubsection}{\numberline {2.0.7}TRACE}{3}
\contentsline {subsubsection}{\numberline {2.0.8}CONNECT}{3}
\contentsline {section}{\numberline {3}Hlavi\IeC {\v c}ky}{3}
\contentsline {section}{\numberline {4}Verze}{3}
\contentsline {subsection}{\numberline {4.1}HTTP 0.9}{3}
\contentsline {subsection}{\numberline {4.2}HTTP 1.0}{3}
\contentsline {subsection}{\numberline {4.3}HTTP 1.1}{4}
\contentsline {subsection}{\numberline {4.4}CGI}{4}
\contentsline {part}{II\hspace {1em}Praktick\IeC {\'a} \IeC {\v c}\IeC {\'a}st}{4}
\contentsline {subsection}{\numberline {4.5}Vyu\IeC {\v z}it\IeC {\'e} knihovny}{4}
\contentsline {subsection}{\numberline {4.6}Syst\IeC {\'e}mov\IeC {\'e} po\IeC {\v z}adavky}{4}
\contentsline {subsection}{\numberline {4.7}Instalace}{4}
\contentsline {subsection}{\numberline {4.8}Konfigurace}{5}
\contentsline {subsection}{\numberline {4.9}Virtual hosty}{5}
\contentsline {subsection}{\numberline {4.10}CGI}{5}
\contentsline {subsection}{\numberline {4.11}Testov\IeC {\'a}n\IeC {\'\i } s programem PuTTY}{5}
\contentsline {subsection}{\numberline {4.12}Protokol}{5}
\contentsline {section}{\numberline {5}Shrnut\IeC {\'\i }}{6}
\contentsline {section}{\numberline {6}P\IeC {\v r}\IeC {\'\i }lohy}{6}
\contentsline {section}{\numberline {7}Reference}{6}
